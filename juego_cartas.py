# -*- coding: utf-8 -*-


class Carta:
    listaPalos = ["Tréboles", "Diamantes", "Corazones", "Espadas"]
    listaValores = ["As", "2", "3", "4", "5",
                    "6", "7", "8", "9", "10", "Sota", "Reina", "Rey"]

    def __init__(self, palo, valor):
        self.palo = palo
        self.valor = valor

    def __str__(self):
        return (self.listaValores[self.valor] + " de " +
                self.listaPalos[self.palo]) + " (%d) " % self.peso()

    def peso(self):
        if self.valor + 1 == 1:
            # As vale 1 como valor por defecto
            return 1
        elif self.valor + 1 > 10:
            # Sota, reina y rey valen 10
            return 10
        else:
            # Simplemente regresamos el valor de la carta
            return (self.valor + 1)


class Mazo:
    def __init__(self):

        # Inicializamos las cartas del mazo
        self.cartas = []
        for palo in range(len(Carta.listaPalos)):
            for valor in range(len(Carta.listaValores)):
                self.cartas.append(Carta(palo, valor))

    def mostrar(self):
        # funcion para imprimir el maso completo
        for carta in self.cartas:
            print carta

    def mezclar(self):
        import random
        nCartas = len(self.cartas)
        for i in range(nCartas):
            j = random.randrange(i, nCartas)
            self.cartas[i], self.cartas[j] = self.cartas[j], self.cartas[i]

    def darCarta(self):
        return self.cartas.pop()

    # Repartimos n Cartas a cada jugador
    def repartir(self, jugadores, nCartas):
        for jugador in jugadores:
            for i in range(nCartas):
                if len(self.cartas) == 0:
                    print "Cero cartas"
                    break
                carta = self.darCarta()
                jugador.agregaCarta(carta)


class Jugador:
    def __init__(self, nombre=""):
        self.cartas = []
        self.nombre = nombre

    def __str__(self):
        return self.nombre

    def agregaCarta(self, carta):
        # Agregamos una carta a su mano
        self.cartas.append(carta)

    def mostrarMano(self):
        s = ""
        for carta in self.cartas:
            s = s + "\t" + str(carta) + "\n"

        print "\nLa mano de %s: \n%s" % (self, s)


class Blackjack:
    def __init__(self):
        self.jugadores = []
        self.mazo = Mazo()
        self.mazo.mezclar()

    def agregarJugador(self, nombre):
        nuevoJugador = Jugador(nombre)
        self.jugadores.append(nuevoJugador)
        

def usuarioAgregaJugadores(juego):
    numJugadores = int(raw_input("Cuántos jugadores son? : "))
    for i in range(numJugadores):
        nombre = raw_input("Nombre del jugador %d: " % i)
        juego.agregarJugador(nombre)

def rondas(juego):

    numJugadores = len(juego.jugadores)

    # Inicializamos la lista de control de turnos
    jugadorPasaTurno = []

    for jugador in juego.jugadores:
        jugadorPasaTurno.append(False)


    # Hacer ronda hasta que nadie tome mas cartas

    while(not all(jugadorPasaTurno)):
        for turnoJugador in range(numJugadores):
            jugador = juego.jugadores[turnoJugador]
            jugador.mostrarMano()
            pasaTurno = raw_input("%s, ¿Deseas pedir una carta mas? (s/n) : " % jugador)
            if pasaTurno.lower() == 's':
                jugador.agregaCarta(juego.mazo.darCarta())
                pasaTurno = False
            else:
                pasaTurno = True
            jugadorPasaTurno[turnoJugador] = pasaTurno


def determinarGanador(juego):

    # Determinar ganador

    numJugadores = len(juego.jugadores)

    puntuaciones = []
    for jugador in juego.jugadores:
        puntJugador = 0

        # Ordenar cartas de jugador descendentemente por peso
        jugador.cartas.sort(key=lambda x: x.peso(), reverse=True)

        # Sacamos los ases y los metemos a una nueva lista y sumamos los pesos de los demas
        ases = []
        for index, carta in enumerate(jugador.cartas):
            if carta.peso() == 1:
                ases.append(jugador.cartas.pop(index))
                puntTemp = carta.peso()
            else:
                puntJugador = puntJugador + carta.peso()


        # lógica para tomar as como 1 u 11

        # Si su puntuación es válida
        if puntJugador <= 21:

            # Si existen ases en su mano
            if len(ases):

                # Sumamos como mínimo 1 por cada as a su puntuacion
                puntJugador = puntJugador + len(ases)

                for As in ases:
                    diff = 21 - puntJugador
                    if diff >= 10:
                        # Tomamos ese as como 11 (resta sumar los 10 faltantes)
                        puntJugador = puntJugador + 10
                        # descartamos ese as
                        ases.pop()

        # Creamos una lista con cada jugador y su puntuacion
        puntuaciones.append({'jugador': str(jugador), 'puntuacion': puntJugador})

    # Imprimimos los puntajes finales
    for p in puntuaciones: print p

    # Descartamos puntos mayores a 21
    puntos = [x['puntuacion'] for x in puntuaciones if x['puntuacion'] <= 21]

    # Buscamos el jugador con mas puntos
    return (item for item in puntuaciones if item["puntuacion"] == max(puntos)).next()



def main():
    juego = Blackjack()

    usuarioAgregaJugadores(juego)


    print "\nMezclando cartas ..."
    juego.mazo.mezclar()
    print "Repartiendo cartas ...\n"
    juego.mazo.repartir(juego.jugadores, 2)


    rondas(juego)


    print "Las manos finales son:"
    for jugador in juego.jugadores:
        jugador.mostrarMano()

    winner = determinarGanador(juego)
    print "\n\nEl ganador es %s" % winner['jugador']


if __name__ == "__main__":
    main()
